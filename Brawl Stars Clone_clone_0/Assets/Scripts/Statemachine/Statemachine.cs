using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Statemachine : NetworkBehaviour
{
    State currentState = null;

    public void SwitchState(State newState)
    {
        currentState?.Exit();
        currentState = newState;
        currentState?.Enter();
    }

    private void Update()
    {
        currentState?.Tick();
    }

    private void FixedUpdate()
    {
        currentState?.PhysicsTick();
    }
}
