using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(InputReader), typeof(Animator))]
public class PlayerStatemachine : Statemachine  
{
    public Rigidbody Rigidbody { get; private set; }
    public InputReader InputReader { get; private set; }
    public Animator Animator { get; private set; }

    [field: SerializeField] public float MoveSpeed { get; private set; }
    [field: SerializeField] public float AccelerationRate { get; private set; }
    [field: SerializeField] public float DeccelerationRate { get; private set; }

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        InputReader = GetComponent<InputReader>();
        Animator = GetComponent<Animator>();
    }

    private void Start()
    {
        if (!IsOwner) return;

        SwitchState(new PlayerFreeLookState(this));
    }

    public override void OnNetworkSpawn()
    {
        if (!IsOwner) return;

        PlayerFollowCamera followCam = Camera.main.GetComponent<PlayerFollowCamera>();
        followCam.FollowTransform(transform);
    }
}
