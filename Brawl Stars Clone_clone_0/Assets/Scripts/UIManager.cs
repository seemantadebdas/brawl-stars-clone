using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] Button hostButton = null;
    [SerializeField] Button serverButton = null;
    [SerializeField] Button clientButton = null;

    private void Awake()
    {
        GameObject buttonPanel = hostButton.transform.parent.gameObject;

        hostButton.onClick.AddListener(() =>
        {
            if (NetworkManager.Singleton.StartHost())
            {
                buttonPanel.SetActive(false);
            }
        });

        serverButton.onClick.AddListener(() =>
        {
            if (NetworkManager.Singleton.StartServer())
            {
                buttonPanel.SetActive(false);
            }
        });

        clientButton.onClick.AddListener(() =>
        {
            if (NetworkManager.Singleton.StartClient())
            {
                buttonPanel.SetActive(false);
            }
        });
    }
}
