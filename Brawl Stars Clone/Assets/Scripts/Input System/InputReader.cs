using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputReader : MonoBehaviour,Controls.IPlayerActions
{
    Controls controls;

    public Vector2 MoveInput { get; private set; }

    public event Action AttackAction;

    private void Start()
    {
        controls = new Controls();
        controls.Player.SetCallbacks(this);

        controls.Player.Enable();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        MoveInput = context.ReadValue<Vector2>();
    }

    public void OnAttack(InputAction.CallbackContext context)
    {
        if (!context.performed) return;
        AttackAction?.Invoke();
    }
    private void OnDisable()
    {
        controls.Player.Disable();
    }
}
