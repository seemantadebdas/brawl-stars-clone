using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class GrassMaterialModifier : NetworkBehaviour
{
    Renderer matRenderer;

    private void Awake()
    {
        matRenderer = GetComponent<Renderer>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player") && other.GetComponent<NetworkObject>().IsOwner)
        {
            SetAlpha(0.5f);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Player") && other.GetComponent<NetworkObject>().IsOwner)
        {
            SetAlpha();
        }
    }

    void SetAlpha(float newAlphaValue = 1f)
    {
        Color materialColor = matRenderer.material.color;
        materialColor.a = newAlphaValue;
        matRenderer.material.color = materialColor;
    }
}
