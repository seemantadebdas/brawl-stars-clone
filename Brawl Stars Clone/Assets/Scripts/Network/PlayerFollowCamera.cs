using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollowCamera : MonoBehaviour
{
    CinemachineVirtualCamera virtualCam = null;

    private void Awake()
    {
        virtualCam = GetComponent<CinemachineVirtualCamera>();
    }

    public void FollowTransform(Transform followTransform)
    {
        virtualCam.Follow = followTransform;
    }
}
