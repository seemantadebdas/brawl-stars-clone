using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerMaterialModifier : NetworkBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("GrassBlock") && !IsOwner)
        {
            Transform visual = transform.GetChild(0);
            visual.gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("GrassBlock") && !IsOwner)
        {
            Transform visual = transform.GetChild(0);
            visual.gameObject.SetActive(true);
        }
    }
}
