using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public abstract class PlayerBaseState : State
{
    protected PlayerStatemachine statemachine;
    public PlayerBaseState(PlayerStatemachine statemachine)
    {
        this.statemachine = statemachine;
    }

    protected void MovePlayer()
    {
        float inputX = statemachine.InputReader.MoveInput.x;
        float inputY = statemachine.InputReader.MoveInput.y;
        Vector3 movementInput = statemachine.transform.forward * inputY + statemachine.transform.right * inputX;

        Vector3 targetVelocity = movementInput * statemachine.MoveSpeed;
        Vector3 velocityDifference = targetVelocity - statemachine.Rigidbody.velocity;

        float accelerationRate = movementInput.magnitude > 0.01f ? statemachine.AccelerationRate : statemachine.DeccelerationRate;

        Vector3 forceDirection = velocityDifference * accelerationRate;

        statemachine.Rigidbody.AddForce(forceDirection);
    }

    protected void LookAtMouse()
    {
        Vector2 mouseScreenPosition = Mouse.current.position.ReadValue();
        Ray ray = Camera.main.ScreenPointToRay(mouseScreenPosition);

        int ignoreRaycastLayer = 1 << 2;
        ignoreRaycastLayer |= 1 << 6;

        if (Physics.Raycast(ray, out RaycastHit hitInfo, 50f, ~ignoreRaycastLayer))
        {
            statemachine.transform.LookAt(hitInfo.point);
        }
    }

    protected float GetNormalizedTime(int animLayer, string animTag)
    {
        AnimatorStateInfo currentStateInfo = statemachine.Animator.GetCurrentAnimatorStateInfo(animLayer);
        AnimatorStateInfo nextStateInfo = statemachine.Animator.GetNextAnimatorStateInfo(animLayer);

        if (statemachine.Animator.IsInTransition(animLayer) && nextStateInfo.IsTag(animTag))
        {
            return nextStateInfo.normalizedTime;
        }
        else if(!statemachine.Animator.IsInTransition(animLayer) && currentStateInfo.IsTag(animTag))
        {
            return currentStateInfo.normalizedTime;
        }
        else
        {
            return 0f;
        }
    }
}
