using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerFreeLookState : PlayerBaseState
{
    readonly int locomotionBlendTreeHash = Animator.StringToHash("LocomotionBlendTree");
    readonly int rifleIdleHash = Animator.StringToHash("RifleIdle");

    readonly int forwardFloatHash = Animator.StringToHash("ForwardFloat");
    readonly int sideFloatHash = Animator.StringToHash("SideFloat");

    const float animTransitionSmoothVal = 0.1f;

    public PlayerFreeLookState(PlayerStatemachine statemachine) : base(statemachine)
    {
    }

    public override void Enter()
    {
        statemachine.InputReader.AttackAction += () =>
        {
            statemachine.SwitchState(new PlayerShootingState(statemachine));
        };

        statemachine.Animator.CrossFadeInFixedTime(locomotionBlendTreeHash, animTransitionSmoothVal, 0);
        statemachine.Animator.CrossFadeInFixedTime(rifleIdleHash, animTransitionSmoothVal, 1);
    }

    public override void Tick()
    {
        LookAtMouse();
        HandleAnimation();
    }


    public override void PhysicsTick()
    {
        MovePlayer();
    }

    public override void Exit()
    {
    }


    private void HandleAnimation()
    {
        float smoothDampValue = 0.15f;
        float inputX = statemachine.InputReader.MoveInput.x;
        float inputY = statemachine.InputReader.MoveInput.y;

        statemachine.Animator.SetFloat(sideFloatHash, inputX, smoothDampValue, Time.deltaTime);
        statemachine.Animator.SetFloat(forwardFloatHash, inputY, smoothDampValue, Time.deltaTime);
    }
}
