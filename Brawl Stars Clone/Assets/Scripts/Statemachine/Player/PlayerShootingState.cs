using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShootingState : PlayerBaseState
{
    readonly int shootingHash = Animator.StringToHash("Shooting");

    const float animTransitionSmoothVal = 0.1f;

    public PlayerShootingState(PlayerStatemachine statemachine) : base(statemachine){}

    public override void Enter()
    {
        statemachine.Animator.CrossFadeInFixedTime(shootingHash, animTransitionSmoothVal, 1);
    }

    public override void PhysicsTick()
    {
        MovePlayer();
    }

    public override void Tick()
    {
        if(GetNormalizedTime(1, "Shoot") > 1f)
        {
            statemachine.SwitchState(new PlayerFreeLookState(statemachine));
            return;
        }
    }

    public override void Exit()
    {
       
    }

}
